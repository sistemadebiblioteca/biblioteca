package biblioteca;
import java.sql.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.mysql.jdbc.PreparedStatement;
public class Conexion {
	private java.sql.Connection conn;
	private java.sql.PreparedStatement pstmt;
	
	public void conectar() {
		try {
		
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection( "jdbc:sqlite:BD.sqlite");
			System.out.print("Conexion realizada");
		} catch (Exception e) {e.printStackTrace();}		
	}
	public void desconecta(){
		try {conn.close();} catch (SQLException e) {e.printStackTrace();
		JOptionPane.showMessageDialog(null,"Error al Cerrar la Base de datos");
		}
	}
  
	public void CrearTabla() {
		
		try {
			pstmt= conn.prepareStatement("DROP TABLE IF EXISTS libros");
			pstmt.execute();
	      } catch (SQLException e) { e.printStackTrace();}
	      
	       try{
	      
			pstmt=conn.prepareStatement( "CREATE TABLE libros ( Codigo int(4) NOT NULL, Nombre_Ejemplar varchar(60) NOT NULL, Editorial varchar(25) NOT NULL, Genero varchar(20) NOT NULL, Autor varchar(25) NOT NULL, Pais_Autor varchar(20) NOT NULL, Nro_Paginas int(5) NOT NULL, A�o int(5) NOT NULL, Precio int(20) NOT NULL)");
			pstmt.execute();
			JOptionPane.showMessageDialog(null,"Base de datos creado con exito");
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null,"Error al crear tabla en Base de Datos",null,0);
		}
		
		
	}
	
	public boolean insertar( modelo md) {
		boolean exito=true;
		try {
			pstmt= conn.prepareStatement("insert into libros (Codigo, Nombre_Ejemplar, Editorial,Genero,Autor, Pais_Autor,Nro_Paginas,A�o,Precio) values (?,?,?,?,?,?,?,?,?)");
			pstmt.setInt(1, md.getCodigol());
			pstmt.setString(2, md.getNombrel());
			pstmt.setString(3, md.getEditorial());
			pstmt.setString(4, md.getGenero());
			pstmt.setString(5, md.getAutor());
			pstmt.setString(6, md.getPaisa());
			pstmt.setInt(7, md.getNpaginas());
			pstmt.setInt(8, md.getA�oe());
			pstmt.setDouble(9, md.getPreciol());
			pstmt.execute();
		} catch (SQLException e) {	e.printStackTrace(); exito=false;
			JOptionPane.showMessageDialog(null,"Error al ingresar ");
		}
		return exito;
	}
	public modelo consultar(int codigol) {
		modelo ObjR=new modelo();
		ResultSet rs;
		try {
			pstmt= conn.prepareStatement("select * from  libros where Codigo=?");
		    pstmt.setInt(1, codigol);
		    rs=pstmt.executeQuery();
			if (rs.next()){
				ObjR.setCodigol(rs.getInt("Codigo"));
				ObjR.setNombrel(rs.getString("Nombre_Ejemplar"));
				ObjR.setEditorial(rs.getString("Editorial"));
				ObjR.setGenero(rs.getString("Genero"));
				ObjR.setAutor(rs.getString("Autor"));
				ObjR.setPaisa(rs.getString("Pais_Autor"));
				ObjR.setNpaginas(rs.getInt("Nro_Paginas"));
				ObjR.setA�oe(rs.getString("A�o"));
				ObjR.setPreciol(rs.getDouble("Precio"));
					
				return ObjR;
			}
		} catch (SQLException e) {	e.printStackTrace();
		JOptionPane.showMessageDialog(null,"Fallo en la consulta de Base de datos");
		}
		return null;
	}
	public ResultSet consultarTodo() {
		ResultSet rs=null;
		try {
			pstmt=(PreparedStatement) conn.prepareStatement("select * from  libros");
		    rs=pstmt.executeQuery();
		    		
		} catch (SQLException e) {	e.printStackTrace();
		JOptionPane.showMessageDialog(null,"Consulta a Base de datos Fallida");
		}
		return rs;
	}
	public boolean actualizar(modelo Datos) {
		boolean exito=true;
		try {
			pstmt= conn.prepareStatement("update libros set  Nombre_Ejemplar=?, Editorial=?, Genero=?, Autor=?,Pais_Autor=?, Nro_Paginas=?, A�o=?,Precio=?  where Codigo=?");
			pstmt.setString(1, Datos.getNombre());
			pstmt.setString(2, Datos.getEditorial());
			pstmt.setString(3, Datos.getGenero());
			pstmt.setString(4, Datos.getAutor());
			pstmt.setString(5, Datos.getPaisa());
			pstmt.setInt(6, Datos.getNpaginas());
			pstmt.setInt(7, Datos.getA�oe());
			pstmt.setDouble(8, Datos.getPreciol());
			pstmt.execute();
		} catch (SQLException e) {	e.printStackTrace(); exito=false;
		JOptionPane.showMessageDialog(null,"Error de Actualizacion en Base de Datos");
		}
		return exito;
	}
	public boolean eliminar(int codigol) {
		boolean exito=true;
	try {
		pstmt= conn.prepareStatement("delete from libros where Codigo=?");
		pstmt.setInt(1, codigol);
		pstmt.execute();
	} catch (SQLException e) {	e.printStackTrace();exito=false;
		JOptionPane.showMessageDialog(null,"Error de Eliminaci�n en Base de Datos");
	}
	return exito;	
	}
	
public void CrearTabla2() {
		
		try {
			pstmt= conn.prepareStatement("DROP TABLE IF EXISTS usuarios");
			pstmt.execute();
	      } catch (SQLException e) { e.printStackTrace();}
	      
	       try{
	      
			pstmt=conn.prepareStatement( "CREATE TABLE usuarios ( Codigo_U int AUTO_INCREMENT, Cedula int(15) NOT NULL, Nombre varchar(25) NOT NULL, Apellido varchar(20) NOT NULL, Domicilio varchar(25) NOT NULL, Poblacion varchar(20) NOT NULL, Provincia varchar(30) NOT NULL, FechaNacimiento date NOT NULL)");
			pstmt.execute();
			JOptionPane.showMessageDialog(null,"Base de datos creado con exito");
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null,"Error al crear tabla en Base de Datos",null,0);
		}
		
		
	}
	
	
	public boolean insertar2( modelo md) {
		boolean exito=true;
		try {
			pstmt= conn.prepareStatement("insert into usuarios (Cedula, Nombre, Apellido,Domicilio, Poblacion,Provincia,FechaNacimiento) values (?,?,?,?,?,?,?)");
			pstmt.setInt(1, md.getCedula());
			pstmt.setString(2, md.getNombre());
			pstmt.setString(3, md.getApellido());
			pstmt.setString(4, md.getDomicilio());
			pstmt.setString(5, md.getPoblacion());
			pstmt.setString(6, md.getProvincia());
			pstmt.setString(7, md.getFechadn());
			pstmt.execute();
		} catch (SQLException e) {	e.printStackTrace(); exito=false;
			JOptionPane.showMessageDialog(null,"Error al ingresar ");
		}
		return exito;
	}
	public modelo consultar2(int cedula) {
		modelo ObjR=new modelo();
		ResultSet rs;
		try {
			pstmt= conn.prepareStatement("select * from  usuarios where Cedula=?");
		    pstmt.setInt(1,cedula);
		    rs=pstmt.executeQuery();
			if (rs.next()){
				ObjR.setCedula(rs.getInt("Cedula"));
				ObjR.setNombre(rs.getString("Nombre"));
				ObjR.setApellido(rs.getString("Apellido"));
				ObjR.setDomicilio(rs.getString("Domicilio"));
				ObjR.setPoblacion(rs.getString("Poblacion"));
				ObjR.setProvincia(rs.getString("Provincia"));
				ObjR.setFechadn(rs.getString("FechaNacimiento"));	
				return ObjR;
			}
		} catch (SQLException e) {	e.printStackTrace();
		JOptionPane.showMessageDialog(null,"Fallo en la consulta de Base de datos");
		}
		return null;
	}
	public ResultSet consultarTodo2() {
		ResultSet rs=null;
		try {
			pstmt=(PreparedStatement) conn.prepareStatement("select * from  usuarios");
		    rs=pstmt.executeQuery();
		    		
		} catch (SQLException e) {	e.printStackTrace();
		JOptionPane.showMessageDialog(null,"Consulta a Base de datos Fallida");
		}
		return rs;
	}
	public boolean actualizar2(modelo Datos) {
		boolean exito=true;
		try {
			pstmt= conn.prepareStatement("update Usuarios set  Nombre=?, Apellido=?, Domicilio=?, Poblacion=?,Provincia=?, FechaNacimiento=? where Cedula=?");
			pstmt.setString(1, Datos.getNombre());
			pstmt.setString(2, Datos.getApellido());
			pstmt.setString(3, Datos.getDomicilio());
			pstmt.setString(4, Datos.getPoblacion());
			pstmt.setString(5, Datos.getProvincia());
			pstmt.setString(6, Datos.getFechadn());
			pstmt.execute();
		} catch (SQLException e) {	e.printStackTrace(); exito=false;
		JOptionPane.showMessageDialog(null,"Error de Actualizacion en Base de Datos");
		}
		return exito;
	}
	public boolean eliminar2(int cedula) {
		boolean exito=true;
	try {
		pstmt= conn.prepareStatement("delete from usuarios where Cedula=?");
		pstmt.setInt(1,cedula);
		pstmt.execute();
	} catch (SQLException e) {	e.printStackTrace();exito=false;
		JOptionPane.showMessageDialog(null,"Error de Eliminaci�n en Base de Datos");
	}
	return exito;	
	}
	
       public void CrearTabla3() {
		
		try {
			pstmt= conn.prepareStatement("DROP TABLE IF EXISTS prestamos");
			pstmt.execute();
	      } catch (SQLException e) { e.printStackTrace();}
	      
	       try{
	      
			pstmt=conn.prepareStatement( "CREATE TABLE prestamos ( Numero_Pedido int AUTO_INCREMENT, Codigo int NOT NULL, Codigo_U int NOT NULL, FechaS Date  NOT NULL, FechaMD date NOT NULL, FechaD date NOT NULL)");
			pstmt.execute();
			JOptionPane.showMessageDialog(null,"Base de datos creado con exito");
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null,"Error al crear tabla en Base de Datos",null,0);
		}
		
		
	}
	
	
	
	public boolean insertar3( modelo md) {
		boolean exito=true;
		try {
			pstmt= conn.prepareStatement("insert into prestamos (Codigo, Codigo_U, FechaS, FechaMD, FechaD) values (?,?,?,?,?)");
			pstmt.setInt(1, md.getCodigol());
			pstmt.setInt(2, md.getCodigou());
			pstmt.setString(3, md.getFechas());
			pstmt.setString(4, md.getFechamd());
			pstmt.setString(5, md.getFechad());
			pstmt.execute();
		} catch (SQLException e) {	e.printStackTrace(); exito=false;
			JOptionPane.showMessageDialog(null,"Error al ingresar ");
		}
		return exito;
	}
	
	public ResultSet ConsultaT() {
		ResultSet rs=null;
		try {
			pstmt= conn.prepareStatement("select libros.Codigo, libros.Nombre_Ejemplar, usuarios.Codigo_U, usuarios.Nombre, prestamos.Numero_Pedido, prestamos.FechaS, prestamos.FechaD FROM prestamos JOIN libros on prestamos.Codigo=libros.Codigo JOIN usuarios on prestamos.Codigo_U=usuarios.Codigo_U");
			rs=pstmt.executeQuery();
		    		
		} catch (SQLException e) {	e.printStackTrace();
		JOptionPane.showMessageDialog(null,"Consulta de Registros Fallida");
		}
		return rs;
	}
	
}