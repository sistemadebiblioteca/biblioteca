
package biblioteca;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;




public class Vista extends JFrame {

	public JTextField txtcodigol, txtnombrel, txteditorial, txtautor ,  txtgenero ,txtpaisa ,txtnpaginas ,txta�oe ,txtpreciol, txtcodil,txtcodiu,txtfechas,txtfecham,txtfechad ,txtnombre,
 txtapellido, txtcedula,txtdomicilio,txtpoblacion, txtprovincia, txtfechaden;
	public JButton btnI, btnC, btnE, btnM,btnS,btnL, btncrear, btnI2, btnC2, btnE2, btnM2,btnS2,btnL2, btncrear2, btnI3,btnS3,btnL3,btncrear3,btnC3;
	public JTable Tab;
	public String Campos[]={"Codigo de Libro","Nombre del Libro","Codigo del Usuario","Nombre del Usuario","Codigo del Pedido","Fecha de Salida","Fecha de Devolucion"};
	
	public Vista(){
	
		
		
               //1 registro de libros   
		
		JLabel lbltitu=new JLabel("                   REGISTRO DE LIBROS ");
        JLabel lblcodigol=new JLabel("Codigo De Libro: ");
		JLabel lblnombrel=new JLabel("Nombre Del Libro: ");
		JLabel lbleditorial=new JLabel("Editorial: ");
		JLabel lblautor=new JLabel("Autor: ");
		JLabel lblgenero= new JLabel("Genero:");
		JLabel lblpaisa= new JLabel("Pais De Autor:");
		JLabel lblnpaginas= new JLabel("N� De Paginas:");
		JLabel lbla�oe= new JLabel("A�o De Edicion:");
		JLabel lblpreciol= new JLabel("Precio Del Libro:");
		
		 btnI=new JButton("Ingresar");
		 btnC=new JButton("Consultar");
		 btnE=new JButton("Eliminar");
		 btnL=new JButton("Limpiar");
		 btnM=new JButton("Actualizar");
		 btncrear=new JButton("CrearTabla");
		 btnS=new JButton("Salir");
		
		 controlador_libros con= new controlador_libros(this);
	      btnI.addActionListener(con);
		  btnC.addActionListener(con);
		  btnE.addActionListener(con);
		  btnL.addActionListener(con);
		  btnM.addActionListener(con);
		  btncrear.addActionListener(con);
		  btnS.addActionListener(con);
		
		 txtcodigol= new  JTextField(10);
		 txtnombrel= new  JTextField(10);
		 txteditorial= new  JTextField(10);
		 txtautor=new  JTextField(10);
		 txtgenero= new  JTextField(10);
		 txtpaisa= new  JTextField(10);
		 txtnpaginas= new  JTextField(10);
		 txta�oe= new  JTextField(10);
		 txtpreciol= new  JTextField(10);
		
		
		
		
		JTabbedPane B=new JTabbedPane();
	
		JPanel P1;
		JPanel P2= new JPanel();
		 
		
		P2.setLayout(new GridLayout(11,1));
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lbltitu);
		P2.add(P1);		
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lblcodigol);P1.add(txtcodigol);
		P2.add(P1);
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lblnombrel);P1.add(txtnombrel);
		P2.add(P1);
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lbleditorial);P1.add(txteditorial);
		P2.add(P1);
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lblgenero);P1.add(txtgenero);
		P2.add(P1);
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lblautor);P1.add(txtautor);
		P2.add(P1);
		
		
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lblpaisa);P1.add(txtpaisa);
		P2.add(P1);
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lblnpaginas);P1.add(txtnpaginas);
		P2.add(P1);
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lbla�oe);P1.add(txta�oe);
		P2.add(P1);		
		
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(lblpreciol);P1.add(txtpreciol);
		P2.add(P1);
		
		P1= new JPanel();
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));
		P1.add(btnI);P1.add(btnC);P1.add(btnE); P1.add(btnL);P1.add(btnM);P1.add(btncrear);P1.add(btnS);
		P2.add(P1);	
		P2.setBorder(BorderFactory.createLineBorder(Color.black));
		
                    //2 usuario
		
 
		
		JLabel lbltitux=new JLabel("                   REGISTRO DE USUARIOS ");
        JLabel lblnombre=new JLabel("Nombre: ");
		JLabel lblapellido=new JLabel("Apellido: ");
		JLabel lblcedula=new JLabel("Cedula: ");
		JLabel lbldomicilio=new JLabel("Domicilio: ");
		JLabel lblpoblacion= new JLabel("Poblacion:");
		JLabel lblprovincia= new JLabel("Provincia:");
		JLabel lblfechaden= new JLabel("Fecha De Nacimiento:");
		
		txtnombre= new  JTextField(10);
	    txtapellido= new  JTextField(10);
	    txtcedula= new  JTextField(10);
		txtdomicilio=new  JTextField(10);
		txtpoblacion= new  JTextField(10);
		txtprovincia= new  JTextField(10);
	    txtfechaden= new  JTextField(10);
		
	    btnI2=new JButton("Ingresar");
		btnC2=new JButton("Consultar");
		btnE2=new JButton("Eliminar");
		btnL2=new JButton("Limpiar");
		btncrear2=new JButton("CrearTabla");
		btnM2=new JButton("Actualizar");
		btnS2=new JButton("Salir");
		
		 controlador_usuarios cont= new controlador_usuarios(this);
	      btnI2.addActionListener(cont);
		  btnC2.addActionListener(cont);
		  btnE2.addActionListener(cont);
		  btnL2.addActionListener(cont);
		  btncrear2.addActionListener(cont);
		  btnM2.addActionListener(cont);
		  btnS2.addActionListener(cont);
		
		
		JPanel P5 = new JPanel();
		JPanel P6 = new JPanel();
		
		P6.setLayout(new GridLayout(11,1));
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lbltitux);
		P6.add(P5);		
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lblcedula);P5.add(txtcedula);
		P6.add(P5);
		
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lblnombre);P5.add(txtnombre);
		P6.add(P5);
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lblapellido);P5.add(txtapellido);
		P6.add(P5);
		
		
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lbldomicilio);P5.add(txtdomicilio);
		P6.add(P5);
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lblpoblacion);P5.add(txtpoblacion);
		P6.add(P5);
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lblprovincia);P5.add(txtprovincia);
		P6.add(P5);
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(lblfechaden);P5.add(txtfechaden);
		P6.add(P5);
		
		
		P5= new JPanel();
		P5.setLayout(new FlowLayout(FlowLayout.LEFT));
		P5.add(btnI2);P5.add(btnC2);P5.add(btnE2);P5.add(btnL2);P5.add(btncrear2);P5.add(btnM2);P5.add(btnS2);
		P6.add(P5);
		
		//3 prestamos
		
				JLabel lbltitul=new JLabel("                   REGISTRO DE PRESTAMOS ");
		        JLabel lblcodil=new JLabel("Codigo De Libro: ");
				JLabel lblcodiu=new JLabel("Codigo Del Usuario: ");
				JLabel lblfechas=new JLabel("Fecha De Salida: ");
				JLabel lblfecham=new JLabel("Fecha Maxima Para Devolver: ");
				JLabel lblfechad= new JLabel("Fecha De Devolucion:");
					
				txtcodil= new  JTextField(10);
				txtcodiu= new  JTextField(10);
				txtfechas= new  JTextField(10);
			    txtfecham=new  JTextField(10);
			    txtfechad= new  JTextField(10);		
			    
			    
			    btnI3=new JButton("Ingresar");
				btnL3=new JButton("Limpiar");
				btncrear3=new JButton("CrearTabla");
				btnS3=new JButton("Salir");
				btnC3=new JButton("Registros");
				
				controlador_prestamos contr= new controlador_prestamos(this);
				
			      btnI3.addActionListener(contr);
				  btnL3.addActionListener(contr);
				  btncrear3.addActionListener(contr);
				  btnS3.addActionListener(contr);
				  btnC3.addActionListener(contr);
		     
				 JPanel P3 = new JPanel();
				 JPanel P4 = new JPanel();
				P4.setLayout(new GridLayout(8,1));
				P3= new JPanel();
				P3.setLayout(new FlowLayout(FlowLayout.LEFT));
				P3.add(lbltitul);
				P4.add(P3);		
				
				P3= new JPanel();
				P3.setLayout(new FlowLayout(FlowLayout.LEFT));
				P3.add(lblcodil);P3.add(txtcodil);
				P4.add(P3);
				
				P3= new JPanel();
				P3.setLayout(new FlowLayout(FlowLayout.LEFT));
				P3.add(lblcodiu);P3.add(txtcodiu);
				P4.add(P3);
				
				P3= new JPanel();
				P3.setLayout(new FlowLayout(FlowLayout.LEFT));
				P3.add(lblfechas);P3.add(txtfechas);
				P4.add(P3);
				
				P3= new JPanel();
				P3.setLayout(new FlowLayout(FlowLayout.LEFT));
				P3.add(lblfecham);P3.add(txtfecham);
				P4.add(P3);
				
				
				P3= new JPanel();
				P3.setLayout(new FlowLayout(FlowLayout.LEFT));
				P3.add(lblfechad);P3.add(txtfechad);
				P4.add(P3);
				
				P3= new JPanel();
				P3.setLayout(new FlowLayout(FlowLayout.LEFT));
				P3.add(btnI3);P3.add(btnL3);P3.add(btncrear3);P3.add(btnS3);
				P4.add(P3);
				JPanel PC;
				
				
				JLabel LPC= new JLabel("Consulta de Prestamos");
				LPC.setHorizontalAlignment(JLabel.CENTER);
				String Registros[][]={{"_","_","_","_","_","_","_"}};
				DefaultTableModel Modelo= new DefaultTableModel(Registros,Campos);
				
				Tab=new JTable(Modelo);
				JScrollPane SRegistro = new JScrollPane();
				SRegistro.setViewportView (Tab);
				
				 PC=new JPanel(new GridLayout(2,1));
				 PC.add(LPC);
				 PC.add(SRegistro);
				
				 B.addTab("Libros",P2);
				 B.addTab("Usuarios",P6);
				 B.addTab("Prestamos", P4);
				 B.addTab("Consultas",PC);

				B.setIconAt(0, new ImageIcon("src/Lib0.jpg"));
				B.setIconAt(1, new ImageIcon("src/Usu1.jpg"));
				B.setIconAt(2, new ImageIcon("src/Pre2.jpg"));
				B.setIconAt(3, new ImageIcon("src/tab3.jpg"));
				B.setBackgroundAt(0,Color.WHITE);
				B.setBackgroundAt(1,Color.WHITE);
				B.setBackgroundAt(2,Color.WHITE);
				B.setBackgroundAt(3,Color.WHITE);

		
		
          this.add(B);
		this.setTitle("SISTEMA DE LA BIBLIOTECA");	
		this.setVisible(true);
		this.pack();
		this.setLocationRelativeTo(null);

	}
}
	
	
	
	
	
	

