package biblioteca;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class controlador_usuarios  implements ActionListener{


	private Vista v;
	private modelo Mod;
	private Conexion bd;

	public controlador_usuarios (Vista vista) {
		v=vista;
		Mod= new modelo();
	bd=new Conexion();
	}

	public void limpiar2(){
	    v.txtcedula.setText("");
		v.txtnombre.setText("");
		v.txtapellido.setText("");
		v.txtdomicilio.setText("");
		v.txtpoblacion.setText("");
		v.txtprovincia.setText("");
		v.txtfechaden.setText("");
		}
	
	
	
	public void actionPerformed(ActionEvent r) {

		modelo Mod2= new modelo();
		if (r.getSource().equals(v.btnI2)){
			
			Mod2.setCedula(Integer.parseInt(v.txtcedula.getText()));
			Mod2.setNombre(v.txtnombre.getText());
			Mod2.setApellido(v.txtapellido.getText());
			Mod2.setDomicilio(v.txtdomicilio.getText());
			Mod2.setPoblacion(v.txtpoblacion.getText());
			Mod2.setProvincia(v.txtprovincia.getText());
			Mod2.setFechadn(v.txtfechaden.getText()); 
			
			bd.conectar();
			
			 	
			if (bd.insertar2(Mod2)){
				JOptionPane.showMessageDialog(null, "Registro almacenado con exito");
				this.limpiar2();
			}
			
	      bd.desconecta();
		}
		if (r.getSource().equals(v.btnC2)){
	     bd.conectar();
			this.Mod=null;
			this.Mod=bd.consultar2(Integer.parseInt(v.txtcedula.getText()));
	    bd.desconecta();
			
			if (Mod!=null){
				v.txtnombre.setText(Mod.getNombre());
				v.txtapellido.setText(Mod.getApellido());
				v.txtdomicilio.setText(Mod.getDomicilio());
				v.txtpoblacion.setText(Mod.getPoblacion());
				v.txtprovincia.setText(Mod.getProvincia());
				v.txtfechaden.setText(Mod.getFechadn());
				
			}
			else {
				JOptionPane.showMessageDialog(null, "Registro no encontrado");
			    this.limpiar2();
			}
			
		}
		if (r.getSource().equals(v.btnE2)){
	    bd.conectar();
	    bd.eliminar2((Integer.parseInt(v.txtcedula.getText())));
			JOptionPane.showMessageDialog(null, "Registro eliminado");
			this.limpiar2();
	    bd.desconecta();
		}
		if (r.getSource().equals(v.btnM2)){
		bd.conectar();
			Mod2=null;
			Mod2=bd.consultar2(Integer.parseInt(v.txtcedula.getText()));

			if (Mod2!=null){
				Mod2.setCedula(Integer.parseInt(v.txtcedula.getText()));
				Mod2.setNombre(v.txtnombre.getText());
				Mod2.setApellido(v.txtapellido.getText());
				Mod2.setDomicilio(v.txtdomicilio.getText());
				Mod2.setPoblacion(v.txtpoblacion.getText());
				Mod2.setProvincia(v.txtprovincia.getText());
				Mod2.setFechadn(v.txtfechaden.getText());
				JOptionPane.showMessageDialog(null, "Registro Modificado con exito");
				bd.actualizar2(Mod2);
				bd.desconecta();
		       this.limpiar2();
			}
		}
		if (r.getSource().equals(v.btnS2)){System.exit(0);}
		
		
		if (r.getSource().equals(v.btncrear2)){
			bd.conectar();bd.CrearTabla2();bd.desconecta();}
		
		
		if(r.getSource().equals(v.btnL2)){this.limpiar2();
		}
		}
		
		
		
		
		
		
	}


