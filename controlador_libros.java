
package biblioteca;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class controlador_libros  implements ActionListener {

	private Vista v;
	private modelo Mod;
	private Conexion bd;

	public controlador_libros (Vista vista) {
		v=vista;
		Mod= new modelo();
	bd=new Conexion();
	}

	public void limpiar(){
	    v.txtcodigol.setText("");
		v.txtnombrel.setText("");
		v.txteditorial.setText("");
		v.txtgenero.setText("");
		v.txtautor.setText("");
		v.txtpaisa.setText("");
		v.txtnpaginas.setText("");
		v.txta�oe.setText("");
		v.txtpreciol.setText("");	
		}
	public void actionPerformed(ActionEvent a) {
		
		modelo Mod2= new modelo();
		
		
		if (a.getSource().equals(v.btnI)){
			
			Mod2.setCodigol(Integer.parseInt(v.txtcodigol.getText()));
			Mod2.setNombrel(v.txtnombrel.getText());
			Mod2.setEditorial(v.txteditorial.getText());
			Mod2.setGenero(v.txtgenero.getText());
			Mod2.setAutor(v.txtautor.getText());
			Mod2.setPaisa(v.txtpaisa.getText());
			Mod2.setNpaginas(Integer.parseInt(v.txtnpaginas.getText())); 
			Mod2.setA�oe(Integer.parseInt(v.txta�oe.getText()));
			Mod2.setPreciol(Integer.parseInt(v.txtpreciol.getText()));
			bd.conectar();
			
			 	
			if (bd.insertar(Mod2)){
				JOptionPane.showMessageDialog(null, "Registro almacenado con exito");
				this.limpiar();
			}
			
	      bd.desconecta();
		}
		if (a.getSource().equals(v.btnC)){
	     bd.conectar();
			this.Mod=null;
			this.Mod=bd.consultar(Integer.parseInt(v.txtcodigol.getText()));
	    bd.desconecta();
			
			if (Mod!=null){
				v.txtnombrel.setText(Mod.getNombrel());
				v.txteditorial.setText(Mod.getEditorial());
				v.txtgenero.setText(Mod.getGenero());
				v.txtautor.setText(Mod.getAutor());
				v.txtpaisa.setText(Mod.getPaisa());
				v.txtnpaginas.setText(Integer.toString(Mod.getNpaginas()));
				v.txta�oe.setText(Integer.toString(Mod.getA�oe()));
				v.txtpreciol.setText(Integer.toString((int) Mod.getPreciol()));
				
			
			}
			else {
				JOptionPane.showMessageDialog(null, "Registro no encontrado");
			    this.limpiar();
			}
			
		}
		if (a.getSource().equals(v.btnE)){
	    bd.conectar();
	    bd.eliminar((Integer.parseInt(v.txtcodigol.getText())));
			JOptionPane.showMessageDialog(null, "Registro eliminado");
			this.limpiar();
	    bd.desconecta();
		}
		if (a.getSource().equals(v.btnM)){
		bd.conectar();
			Mod2=null;
			Mod2=bd.consultar(Integer.parseInt(v.txtcodigol.getText()));

			if (Mod2!=null){
				Mod2.setCodigol(Integer.parseInt(v.txtcodigol.getText()));
				Mod2.setNombrel(v.txtnombrel.getText());
				Mod2.setEditorial(v.txteditorial.getText());
				Mod2.setGenero(v.txtgenero.getText());
				Mod2.setAutor(v.txtautor.getText());
				Mod2.setPaisa(v.txtpaisa.getText());
				Mod2.setNpaginas(Integer.parseInt(v.txtnpaginas.getText()));
				Mod2.setA�oe(v.txta�oe.getText());
				Mod2.setPreciol(Integer.parseInt(v.txtpreciol.getText()));
				JOptionPane.showMessageDialog(null, "Registro Modificado con exito");
				bd.actualizar(Mod2);
				bd.desconecta();
		       this.limpiar();
			}
		}
		if (a.getSource().equals(v.btnS)){System.exit(0);}
		
		if (a.getSource().equals(v.btncrear)){
			bd.conectar();bd.CrearTabla();bd.desconecta();}
		
	
		if(a.getSource().equals(v.btnL)){this.limpiar();
		}
		}
		
	}
	
